
import unittest
from utilities.shell import safe_check_output_with_status
from constants import BIN_FOLDER, MAESTRO_ROOT
from tests.path import TURTLE_ME_PATH, OPERATIONAL_HOME, PARALLEL_HOME, OPERATIONAL_SUITES_HOME

"""
Run shell commands to test various CLI in this project.
"""


class TestCli(unittest.TestCase):
    
    def test_basic_success(self):
        """
        All of these bash commands should exit with a success status.
        """
        h=BIN_FOLDER+"heimdall"
        
        home_options=" --op-home={op_home} --op-suites-home={op_suites_home} --par-home={par_home}"
        home_options=home_options.format(op_home=OPERATIONAL_HOME,
                                         par_home=PARALLEL_HOME,
                                         op_suites_home=OPERATIONAL_SUITES_HOME)
        
        commands=["%s --exp=%s %s"%(h,TURTLE_ME_PATH,home_options),
                  "%s blame %s"%(h,MAESTRO_ROOT),
                  "%s deltas %s %s"%(h,TURTLE_ME_PATH,home_options)]
        
        for command in commands:
            output,status=safe_check_output_with_status(command)
            message="\ncommand =\n%s\n\noutput =\n%s"%(command,output)
            self.assertEqual(status,0,msg=message)