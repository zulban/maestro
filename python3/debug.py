#!venv/bin/python3

from constants import *
from utilities import *
from tests.path import *
from heimdall import *
from maestro_experiment import *
from heimdall.path import *

context=SCANNER_CONTEXT.TEST
context=SCANNER_CONTEXT.OPERATIONAL
path=SUITES_WITHOUT_CODES+"b026"
path=G1_MINI_ME_PATH
path=SUITES_WITHOUT_CODES+"b006"
path=SUITES_WITH_CODES+"e026"
path=SUITES_WITH_CODES+"i001"

scanner=ExperimentScanner(path,
                          context=context,
                          operational_home=OPERATIONAL_HOME,
                          parallel_home=PARALLEL_HOME,
                          operational_suites_home=OPERATIONAL_SUITES_HOME,
                          debug_hub_filecount=10,
                          debug_hub_ignore_age=True)

whitelist=["i001","w005"]
scanner.print_report(level="b",max_repeat=3,whitelist=whitelist)