# This script requires more than 1MB of memory

import string

a = ""
while len(a) < 10**12:
    a += string.ascii_lowercase
