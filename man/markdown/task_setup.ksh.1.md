task_setup.ksh -- (deprecated)
=============================================

`task_setup.ksh` is deprecated. See `task_setup.dot` for the up to date utility.
