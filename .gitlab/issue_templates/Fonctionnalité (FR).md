### Résumé

<!-- (Résumez votre idée de fonctionnalité en une ou deux phrases.) -->

### Public cible

<!-- (Pour qui faisons-nous cela? Quels groupes ou quelles personnes? Si possible, utilisez le caractère '@' pour les inclure dans cet enjeu.) -->

### Détails

<!-- (Inclure les cas d'utilisation, les avantages et / ou les objectifs) -->

### Étapes de développement (facultatif)

<!-- (Si vous le pouvez, décrivez les étapes de développement nécessaires pour implémenter cette fonctionnalité) -->

<!-- Laissez ceci ici pour que l'enjeu soit automatiquement catégorisé comme une fonctionnalité -->
/label ~type:feature
