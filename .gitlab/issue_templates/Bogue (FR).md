### Résumé

<!-- (Résumez le bogue rencontré en une ou deux phrases) -->

### Étapes à suivre pour reproduire

<!-- (Comment reproduisons-nous ce bogue? Remarque: si le développeur ne peut pas reproduire votre bogue avec vos étapes exactes, votre bogue ne peut pas être corrigé.) -->

### Comportement attendu

<!-- (Quel est le comportement correct attendu?) -->

### Comportement actuel

<!-- (Ce qui s'est réellement passé) -->

### Détails, journaux, code et captures d'écran (facultatif)

<!--
Fournissez des informations supplémentaires ici. Si vous écrivez du code, utilisez des blocs de code, qui sont entourés de trois ticks (la touche tick est à côté de la touche "numéro un" de votre clavier). Comme ça:

`` `
print ("bonjour")
`` `

Ceci est tout écrit dans markdown. Pour apprendre les bases du markdown, regardez cette vidéo de 5 minutes:

https://www.youtube.com/watch?v=SCAfcuQ0dBE
-->



<!-- Laissez ceci ici pour que l'enjeu soit automatiquement identifié comme un bogue -->
/label ~type:bug
