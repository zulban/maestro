



global MAESTRO_BIN
global MAESTRO_MANAGER_SRC
global env
global MUSER 
global array ExperimentInode
global array ArrayTabsDepot
global ListAllExperiments
global List_Exps
set List_Exps {}
set ListAllExperiments {}

set MAESTRO_BIN $env(MAESTRO_BIN)
set MAESTRO_MANAGER_SRC $env(MAESTRO_MANAGER_SRC)

# -- get user
set MUSER [exec id -nu]

namespace eval XPManager {

    global MAESTRO_BIN MAESTRO_MANAGER_SRC

    variable _wfont

    variable notebook
    variable mainframe
    variable MCGfrm
    variable status
    variable prgtext
    variable prgindic -1
    variable progmsg
    variable progval
    variable _progress 0
    variable _afterid  ""
    variable _status "Compute in progress..."
    variable font
    variable font_name
    variable f0
    variable _version "1.0"
    variable EntryPoint no-selection
    variable ExpOpsRepository
    variable ExpParRepository
    variable ExpPreOpsRepository

    # -- buttons icones
    foreach img {bug XpSel FoldXp Tool Refresh Ok Cancel Close Add Stop Remove Save Next Previous Apply Help Quit Notify Up font} {
               eval variable img_$img [image create photo -file ${MAESTRO_MANAGER_SRC}/etc/images/$img.gif]
    }

    # -- Audit
    variable img_clsdFolderImg     [image create photo -file ${MAESTRO_MANAGER_SRC}/etc/images/clsdFolder.gif]
    variable img_openFolderImg     [image create photo -file ${MAESTRO_MANAGER_SRC}/etc/images/openFolder.gif]
    variable img_fileImg           [image create photo -file ${MAESTRO_MANAGER_SRC}/etc/images/file.gif]

    # -- Palacard
    variable img_placard [image create photo -file ${MAESTRO_MANAGER_SRC}/etc/images/placard.gif]
    
    # -- Exp Icons
    variable img_ExpIcon           [image create photo -file ${MAESTRO_MANAGER_SRC}/etc/images/xp.gif]
    variable img_ExpNoteIcon       [image create photo -file ${MAESTRO_MANAGER_SRC}/etc/images/xp.note.gif]
    variable img_ExpSunny          [image create photo -file ${MAESTRO_MANAGER_SRC}/etc/images/Sunny.gif]
    variable img_ExpThunder        [image create photo -file ${MAESTRO_MANAGER_SRC}/etc/images/Thunder.gif]
    variable img_ExpThunderstorms  [image create photo -file ${MAESTRO_MANAGER_SRC}/etc/images/Thunderstorms.gif]

    foreach script {Dialogs.tcl XTree.tcl TreeUtil.tcl NewExp.tcl Audit.tcl Import.tcl XpBrowser.tcl SubmitBug.tcl About.tcl} {
	namespace inscope :: source ${MAESTRO_MANAGER_SRC}/lib/x_manager/$script
    }

    foreach script { Preferences.tcl XpOptions.tcl dkffont.tcl } {
	namespace inscope :: source ${MAESTRO_MANAGER_SRC}/lib/common/$script
    }
   
    # this is for the exp's configs *.cfg files
     namespace inscope :: source ${MAESTRO_MANAGER_SRC}/lib/f_manager/ExpModTreeView.tcl
}

proc XPManager::create { {startup_exp ""} } {
    global MAESTRO_BIN MAESTRO_MANAGER_SRC MUSER startupExp
    
    variable _wfont
    variable notebook
    variable mainframe
    variable MCGfrm

    variable font
    variable prgtext "Please wait while loading font..."
    variable prgindic -1
    variable f0
    variable EntryPoint
    variable ExpOpsRepository
    variable ExpParRepository
    variable ExpPreOpsRepository
 
    _create_intro
    update

    SelectFont::loadfont
     
    bind all <F12> { catch {console show} }
   

   if { [info exists ::env(CMCLNG)] == 0 || [string compare "$::env(CMCLNG)" "english"] == 0 } {
              source ${MAESTRO_MANAGER_SRC}/lib/x_manager/menu_english.tcl
   } else {
              source ${MAESTRO_MANAGER_SRC}/lib/x_manager/menu_francais.tcl
   }

   set prgtext   "Creating MainFrame..."
   set prgindic  0
   
   set mainframe [MainFrame .mainframe \
                       -menu         $descmenu \
                       -textvariable XPManager::status \
		       -progressfg blue\
                       -progressvar  XPManager::prgindic]

   # -- Parse Config files 
   set  prgtext   "Parsing Config files ..."
   incr prgindic
  
   Preferences::ParseUserMaestrorc

   # -- Check if all the vars All There
   Preferences::set_prefs_default
   Preferences::set_liste_Wall_Papers

   XPManager::ParseOpParExpDepot
  
   # -- Find Op Par Experiments and make them avail. to apl. 
   set  prgtext   "Findind Op, Par Experiments ..."
   incr prgindic

   # --Show Name and Version
   set host [exec hostname]
    
   if {[regexp {afsiops|afsisio|afsipar} $MUSER]} { 
             $mainframe addindicator -text "$MUSER@$host" -bg #4dbaff
   } else {
             $mainframe addindicator -text "$MUSER@$host" -bg Azure
   }

   # --NoteBook creation
   set prgtext   "Creating Notebook..."
   incr prgindic
   
   set MCGfrm    [$mainframe getframe]

   set prgtext   "Creating Browser..."
   incr prgindic
   
   XpBrowser::create $MCGfrm

   if { ${startupExp} != "" } {
      XpBrowser::validateAndShowExp ${startupExp}
   }

   set prgtext   "Done"
   incr prgindic
   
    
   pack $mainframe -fill both -expand yes
   update idletasks
   destroy .intro



}


proc XPManager::update_font { newfont } {
    variable _wfont
    variable notebook
    variable font
    variable font_name

    . configure -cursor watch
    if { $font != $newfont } {
        $_wfont configure -font $newfont
        $notebook configure -font $newfont
        set font $newfont
    }
    . configure -cursor ""
}


proc XPManager::_create_intro { } {
    
    global MAESTRO_BIN MAESTRO_MANAGER_SRC

    set top [toplevel .intro -relief raised -borderwidth 2]

    wm withdraw $top
    wm overrideredirect $top 1


    set ximg  [label $top.x -image $XPManager::img_placard -background white]
    set bwimg [label $ximg.bw -bitmap @${MAESTRO_MANAGER_SRC}/etc/images/xm.xbm -foreground grey90 -background white]
    
    
    set frame [frame $ximg.f -background white]
    set lab1  [label $frame.lab1 -text "Loading ..."  -background white -font {times 8}]
    set lab2  [label $frame.lab2 -textvariable XPManager::prgtext  -background white -font {times 8} -width 35]

    set prg   [ProgressBar $frame.prg -width 50 -height 10 -variable XPManager::prgindic -maximum 5]


    pack $lab1 $lab2 $prg
    place $frame -x 0 -y 0 -anchor nw
    place $bwimg -relx 1 -rely 1 -anchor se
    pack $ximg

    BWidget::place $top 0 0 center
    
    wm deiconify $top
}

proc XPManager::parseCmdOptions {} {
   global env argv startupExp
   set startupExp ""
   if { [info exists argv] } {
      set options {
         {exp.arg "" "exp to be selected at startup"}
      }
   }
   namespace inscope :: package require cmdline
   set usage "\[options] \noptions:"
   if [ catch { array set params [::cmdline::getoptions argv $options $usage] } message ] {

      puts "\n$message"
      exit 1
   }

   if { ! ($params(exp) == "") } {
      puts "Using exp specified at startup: $params(exp)"
      # user specified an exp, use it
      set startupExp $params(exp)
   }

   if { ${startupExp} == "" && [info exists env(SEQ_EXP_HOME)] } {
      # if exp not defined and seq_exp_home defined use it
      puts "Using SEQ_EXP_HOME at startup: $env(SEQ_EXP_HOME)"
      set startupExp $env(SEQ_EXP_HOME)
   }

   if { ${startupExp} == "" } {
      set isExpCheckPath [pwd]/EntryModule
      # at last if pwd is an exp, use it
      if { [file exists ${isExpCheckPath}] && [file type ${isExpCheckPath}] == "link" && [file readable ${isExpCheckPath}] } {
         puts "Using current pwd as exp for startup: [pwd]"
         set startupExp [pwd]
      }
   }

}

proc XPManager::main {} {
   
    global MAESTRO_BIN MAESTRO_MANAGER_SRC

    lappend ::auto_path ${MAESTRO_MANAGER_SRC}
    lappend ::auto_path ${MAESTRO_MANAGER_SRC}/lib/common
    lappend ::auto_path ${MAESTRO_MANAGER_SRC}/lib/f_manager

    namespace inscope :: package require BWidget 1.9

    option add *TitleFrame.l.font {helvetica 11 bold italic}

    wm withdraw .
    wm title . $Dialogs::XPM_ApplicationName

    SharedData_init
    XPManager::create

    BWidget::place . 0 0 center
    wm deiconify .
    wm minsize . 700 500
    raise .
    focus -force .
}

proc XPManager::_show_progress { } {
    variable _progress
    variable _afterid
    variable _status

      
    if { $_progress } {
        set XPManager::status   "In progress..."
        set XPManager::prgindic 0
        $XPManager::mainframe showstatusbar progression
        if { $_afterid == "" } {
            set _afterid [after 10 XPManager::_update_progress]
        }
    } else {
        set XPManager::status ""
        $XPManager::mainframe showstatusbar status
        set _afterid ""
    }
}


proc XPManager::_update_progress { } {

    variable _afterid

    if { $XPManager::_progress } {
        if { $XPManager::prgindic < 100 } {

            puts "prgindic -> $XPManager::prgindic _progress-> $XPManager::_progress"
            set _afterid [after 10 XPManager::_update_progress]
        } else {
            set $XPManager::_progress 0
            $XPManager::mainframe showstatusbar status
            set XPManager::status "Done"
            set _afterid ""
            after 500 {set XPManager::status ""}
        }
    } else {
        set _afterid ""
    }
}

proc XPManager::show_progdlg {w titre} {

    set XPManager::progmsg "$titre ..."
    set XPManager::progval 0

    ProgressDlg ${w}.progress -parent $w -title "Wait..." \
            -type         incremental \
            -width        30 \
            -textvariable XPManager::progmsg \
            -variable     XPManager::progval \
            -stop         "" \
            -command      "destroy ${w}.progress"

}

proc XPManager::update_progdlg {w pn titre} {
    if { [winfo exists ${w}.progress] } {
            set XPManager::progval 5
	    set XPManager::progmsg "$titre  ... $pn"
    } 
}


proc XPManager::ListExperiments {} {
    global ListAllExperiments
    set ListAllExperiments {}
  
    set buf1 {}
    set buf2 {}
}

proc XPManager::ParseOpParExpDepot {} {
           
           global MAESTRO_BIN MAESTRO_MANAGER_SRC

           set prefDparser [interp create -safe]
           $prefDparser alias ExpOpsRepository    XPManager::set_prefs_cmd_ExpOpsRepository
           $prefDparser alias ExpParRepository    XPManager::set_prefs_cmd_ExpParRepository
           $prefDparser alias ExpPreOpsRepository XPManager::set_prefs_cmd_ExpPreOpsRepository
           $prefDparser alias DefaultModDepot XPManager::set_prefs_cmd_DefaultModDepot

           set cmd {
                   set fid [open [file join ${MAESTRO_MANAGER_SRC}/etc/config/ xm.cfg ] r]
                   set script [read $fid]
                   close $fid
                   $prefDparser eval $script
           }
           if {[catch  $cmd err] != 0} {
		
		    puts "Error Parsing file config"
           }

}

proc XPManager::set_prefs_cmd_ExpOpsRepository   {name args} { 
           global array ArrayTabsDepot

           set XPManager::ExpOpsRepository  $name
	   # -- Puts alws by default OP. exps
	   set ArrayTabsDepot($Dialogs::Nbk_OpExp)  $name
}

proc XPManager::set_prefs_cmd_ExpParRepository   {name args} { 
           global array ArrayTabsDepot

           set XPManager::ExpParRepository  $name
	   # -- Puts alws by default OP. exps
	   set ArrayTabsDepot($Dialogs::Nbk_PaExp)   $name
}

proc XPManager::set_prefs_cmd_ExpPreOpsRepository {name args} { 
           global array ArrayTabsDepot

           set XPManager::ExpPreOpsRepository    $name
	   # -- Puts alws by default OP. exps
	   set ArrayTabsDepot($Dialogs::Nbk_PrExp) $name
}

# this is temp.
proc XPManager::set_prefs_cmd_DefaultModDepot {name args} { 
}

# -- Global script
set script {
      if {[%W identify %x %y] == "close_button"} {
               set tabs [$XPManager::notebook tabs] 
               set ind  [%W index @%x,%y] 
               set tab [lindex $tabs $ind]
               destroy $tab
               break
     }
}

proc sleep {N} {
   after [expr {int($N * 1000)}]
}

# -- Prepar widget display language
Dialogs::setDlg


XpOptions::globalOptions
XpOptions::tablelistOptions
XPManager::parseCmdOptions
XPManager::main
wm geom . [wm geom .]

# -- warn if parsing error user .maestrorc file
if { $Preferences::ERROR_PARSING_USER_CONFIG == 1 } {
                     Dialogs::show_msgdlg $Dialogs::Dlg_Error_parsing_user_file  ok warning "" .
}





if {[string compare $Preferences::ListUsrTabs "" ] == 0 } {
                     Dialogs::show_msgdlg $Dialogs::Dlg_DefineExpPath  ok warning "" .
}





