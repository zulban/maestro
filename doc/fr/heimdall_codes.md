[(English)](../en/heimdall_codes.md)

# Heimdall Codes de Message

Cette page liste tous les codes et messages Heimdall. Voir aussi le [Heimdall README](https://gitlab.science.gc.ca/CMOI/maestro/blob/integration/src/python3/HEIMDALL.md).




# Niveau: Critique
![color c image](../color-critical.png)

### [c001](#c001): Aucun EntryModule

Le lien EntryModule n'existe pas: '{entry_module}'


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/EntryModule)



![color c image](../color-critical.png)

### [c002](#c002): EntryModule incorrect

L'EntryModule n'est pas un lien: '{entry_module}'


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/EntryModule)



![color c image](../color-critical.png)

### [c003](#c003): Aucune expérience ici

Impossible de trouver une expérience pour le chemin: '{path}'. Un dossier d'expérience valide contient un lien 'EntryModule'.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/EntryModule)



![color c image](../color-critical.png)

### [c004](#c004): Pas d'EntryModule flow.xml

Le fichier EntryModule requis flow.xml n'existe pas: '{flow_xml}'


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/EntryModule)



![color c image](../color-critical.png)

### [c005](#c005): Mauvais EntryModule flow.xml

Le fichier EntryModule requis flow.xml n'a pas réussi à analyser en tant que XML: '{flow_xml}'


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/EntryModule)



![color c image](../color-critical.png)

# Niveau: Erreur
![color e image](../color-error.png)

### [e001](#e001): Dossiers manquants dans la suite

Toutes les suites ont besoin des dossiers: 'listings', 'sequencing', et 'logs'. Cette suite est manquante: {folders}


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro_Files_and_Folders)



![color e image](../color-error.png)

### [e002](#e002): Mauvais XML

Impossible d'analyser XML '{xml}'



![color e image](../color-error.png)

### [e003](#e003): Nom de nœud invalide

Nom de nœud invalide '{node_name}' en flux XML '{flow_path}'. Les noms de nœuds doivent être écrits en caractères alphanumériques avec des points, des tirets ou des sous-tirets. Ils doivent correspondre à la regex: {regex}


[Plus d'info](https://regex101.com/)



![color e image](../color-error.png)

### [e004](#e004): Lien symbolique rompu

La cible du lien symbolique '{link}' n'existe pas.


[Plus d'info](https://www.google.com/search?q=linux+find+broken+symlinks)



![color e image](../color-error.png)

### [e005](#e005): Flux du module dispersé

Les enfants du flux XML pour le module '{module_name}' sont définis dans plus d'un fichier flow.xml:\n{flow_xmls}


[Plus d'info](https://gitlab.science.gc.ca/CMOI/mflow-prototype/issues/23#note_189290)



![color e image](../color-error.png)

### [e007](#e007): Fichier ressources nécessaire manquant

La tâche '{task_path}' n'a pas de fichier ressources à '{resource_path}'. Il s'agit d'un fichier nécessaire pour le contexte '{context}'.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/folders/resources)



![color e image](../color-error.png)

### [e008](#e008): Accolades manquantes dans un XML ressource

Les variables dans les XML ressources doivent posséder des accolades comme '${{ABC}}' et non '$ABC'. Le fichier '{file_path}' contient:\n{matching_string}


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/folders/resources)



![color e image](../color-error.png)

### [e009](#e009): Nombre impair d'accolades  dans un XML ressource

Un nombre impair d'accolades '{attribute_value}' a été trouvé dans un fichier ressources: '{file_path}'



![color e image](../color-error.png)

### [e010](#e010): Chemin codé en dur dans le fichier de configuration

Le chemin '{bad_path}' défini dans '{config_path}' est absolu et ne commence pas par une variable comme '${{SEQ_EXP_HOME}}'. Cette configuration d'expérience est moins portable et plus fragile.



![color e image](../color-error.png)

### [e011](#e011): Mauvaise dépendance d'expérience

Le chemin de l'attribut  'exp' '{exp_value}' dans le fichier ressources '{resource_path}' ne mène pas à un dossier qui existe.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/folders/resources)



![color e image](../color-error.png)

### [e012](#e012): Variables indéfinies de  XML ressource

Le XML ressource '{resource_path}' utilise des variables qui ne sont pas définies dans les fichiers ressources comme 'resources/resources.def'. Variables:\n{variable_names}


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/folders/resources)



![color e image](../color-error.png)

### [e013](#e013): L'état de diffusion ne correspond pas au contexte

L'expérience semble avoir le contexte '{context}', mais les variables dans '{cfg_path}' ne correspondent pas à ce qui est attendu: {unexpected}


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Dissemination)



![color e image](../color-error.png)

### [e014](#e014): Présence d'éléments qui ne sont pas des liens dans hub

Les expériences avec le contexte '{context}' peuvent seulement avoir des dossiers de liens symboliques dans leur dossier 'hub':\n{bad}


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/folders/hub)



![color e image](../color-error.png)

### [e015](#e015): Valeur ressource dépasse le maximum

La valeur ressource '{value}' pour '{attribute}' dans '{xml_path}' est supérieure au maximum de '{maximum}' défini dans 'jobctl-qstat' pour la file d'attente '{queue}'.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/folders/resources)



![color e image](../color-error.png)

### [e016](#e016): Pas de dépôt git

Il n'y a pas de dépôt git associé à cette expérience. Les expériences avec le contexte '{context}' devraient toujours être sous contrôle de version par git.



![color e image](../color-error.png)

### [e017](#e017): Nom en double dans le conteneur flux

Le contenant '{container_name}' dans un flux XML a plus d'un élément avec le 'name' ou 'sub_name' '{duplicate_name}'. Tous les enfants directs dans un conteneur doivent avoir un name ou sub_name unique.



![color e image](../color-error.png)

### [e018](#e018): Attribut 'name' dans l'élément SUBMITS

Tous les éléments SUBMITS doivent avoir un attribut 'sub_name' et non 'name'. Le fichier '{file_path}' contient:\n{matching_string}


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/SUBMITS)



![color e image](../color-error.png)

### [e019](#e019): Attribut 'sub_name' dans l'élément non-SUBMITS

Seuls les éléments SUBMITS doivent avoir un attribut 'sub_name'. Vous aurez peut-être à utiliser l'attribut 'name'. Le fichier '{file_path}' contient:\n{matching_string}


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/SUBMITS)



![color e image](../color-error.png)

### [e020](#e020): Propriétaires ou autorisations du dossier log irrégulier

La propriété et les autorisations '{ugp}' du dossier du journal '{path}' diffèrent des autres dossiers de journaux. Les permissions des dossiers journaux temporaires devraient être les mêmes: {folders}


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro_Files_and_Folders)



![color e image](../color-error.png)

### [e021](#e021): Définitions de ressources en double

La variable '{variable}' est définie plus d'une fois dans le fichier '{path}'. Des outils tels que getdef peuvent se comporter de manière inattendue.



![color e image](../color-error.png)

### [e022](#e022): Dossier 'stats' manquant

Le dossier 'stats' est manquant, mais nécessaire parce que les variables ressources SEQ_RUN_STATS_ON et SEQ_AVERAGE_WINDOW sont utilisées: '{required}'


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/folders/stats)



![color e image](../color-error.png)

### [e023](#e023): Mauvaise expression en boucle

Le fichier '{path}' a une mauvaise expression en boucle '{loop_expression}'; il devrait utiliser des virgules qui séparent les nombres dans le format 'début:fin:étape:set'.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/1.5.0/Release_Notes#Multi-definition_numeric_loops)



![color e image](../color-error.png)

### [e024](#e024): Mauvaises autorisations sur les dossiers log opérationnels

Le dossier du journal '{folder}' doit avoir les autorisations '{expected}', mais il a '{ugp}'.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro_Files_and_Folders)



![color e image](../color-error.png)

### [e025](#e025): Le lien Realpath est un chemin non opérationnel

L'expérience semble avoir le contexte '{context}', mais le fichier '{path}' contient des références à un utilisateur non opérationnel:\n{bad}



![color e image](../color-error.png)

### [e026](#e026): La chaîne de liens contient un chemin non opérationnel

L'expérience semble avoir le contexte '{context}', mais le fichier '{path}' est une chaîne de liens qui contient un répertoire d'utilisateur home non opérationnel. Le 'realpath' pour le lien est correct, mais le traverser ne l'est pas:\n{bad}



![color e image](../color-error.png)

### [e027](#e027): Mauvaise syntaxe de script shell

La commande '{verify_cmd}' a trouvé des erreurs dans le script shell '{path}'. Sortie:\n{output}



![color e image](../color-error.png)

### [e028](#e028): Dépendance externe est relative

Le fichier '{resource_path}' a un attribut relatif 'dep_name' '{dep_name}' mais 'exp' est externe. Les dépendances externes ne peuvent pas être relatives.



![color e image](../color-error.png)

### [e029](#e029): Jeton associatif manquant

Le fichier '{path}' a un élément DEPENDS_ON avec les attributs '{index}' et '{local_index}' mais seulement 'index' a un jeton associatif comme '$((x))'. Si 'index' en a un, 'local_index' en a besoin aussi.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/Resources#WebX_Slides_and_Videos)



![color e image](../color-error.png)

# Niveau: Avertissment
![color w image](../color-warning.png)

### [w001](#w001): Fichier ressources manquant

La tâche '{task_path}' n'a pas de fichier ressources à '{resource_path}'. Créez un fichier ressources pour éviter d'utiliser les valeurs inconnues par défaut.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/folders/resources)



![color w image](../color-warning.png)

### [w002](#w002): Fiichier ressources manquante pour boucle

La boucle ou un switch '{node_path}' n'a pas de fichier ressources à '{resource_path}'. Créez un fichier ressources pour éviter d'utiliser les valeurs par défaut inconnues.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/folders/resources)



![color w image](../color-warning.png)

### [w003](#w003): site1/site2 obsolètes

Les chemins 'site1' et 'site2' ne sont plus accessibles après les mises à jour du début 2020. Le fichier '{file_path}' contient:\n{matching_string}



![color w image](../color-warning.png)

### [w004](#w004): hall1/hall2 obsolètes

Les chemins 'hall1' et 'hall2' ne sont plus accessibles après les mises à jour du début 2020. Le fichier '{file_path}' contient:\n{matching_string}



![color w image](../color-warning.png)

### [w005](#w005): Plusieurs répertoires home pour le projet

L'expérience maestro se trouve dans le dossier home de '{real_home}', mais les fichiers maestro principaux constituent des liens symboliques vers d'autres utilisateurs. Ceci peut être trop instable pour une installation opérationnelle. Exécutez realpath sur ces liens:\n{bad_links}


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/User:Maciaa/home_references)



![color w image](../color-warning.png)

### [w006](#w006): Chemin codé en dur dans le fichier de configuration

Le chemin '{bad_path}' défini dans '{config_path}' est absolu et ne commence pas par une variable comme '${{SEQ_EXP_HOME}}'. Il s'agit d'une manière moins portable et plus fragile de configurer une expérience.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/SEQ_EXP_HOME)



![color w image](../color-warning.png)

### [w007](#w007): Aucun statut de support dans l'expérience opérationnelle

Il n'y a pas d'attribut 'SupportInfo' dans le XML '{xml_path}', ou bien le XML n'existe pas. Cet attribut est nécessaire pour les expériences opérationnelles.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/ExpOptions.xml)



![color w image](../color-warning.png)

### [w008](#w008): Statuts de support multiples

Le XML '{xml_path}' a plus d'un élément SupportInfo.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/ExpOptions.xml)



![color w image](../color-warning.png)

### [w009](#w009): Faute de frappe dans une variable ressources

La variable ressources '{maybe_typo}' est définie, mais le nom de la variable standard '{expected}' ne l'est pas. Vouliez-vous écrire ceci à la place?


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/User:Lims/projects/tech_transfer_improvements#Using_standard_variables_for_machine_and_queue_definitions)



![color w image](../color-warning.png)

### [w010](#w010): Valeur incorrecte de file d'attente pour variable ressources

La valeur '{value}' pour la variable ressources '{name}' est pas une file d'attente disponible en qstat. Files d'attente:\n{queues}



![color w image](../color-warning.png)

### [w011](#w011): Expérience n'est pas en XML overview

L'expérience semble avoir le contexte '{context}', mais il n'a pas été trouvé dans les expériences de {exp_count} du fichier XML overview: {xml_path}


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/overview_xml)



![color w image](../color-warning.png)

### [w012](#w012): Expérience dans un XML overview inattendu

L'expérience semble avoir le contexte '{context}', mais il n'a pas été trouvé dans les expériences {exp_count} dans le XML overview '{xml_context}: {xml_path}


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/overview_xml)



![color w image](../color-warning.png)

### [w013](#w013): Variables de diffusion égarées

Les variables {variables} appartiennent uniquement à des fichiers 'experiment.cfg', mais ont été trouvées dans: '{cfg_path}'


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Dissemination)



![color w image](../color-warning.png)

### [w014](#w014): Les paires de hub ont des cibles différentes

Les liens de dossiers hub '{folder1}' et '{folder2}' devraient avoir des cibles similaires, mais celles-ci sont en fait très différentes:\n{target1}\n{target2}


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/folders/hub)



![color w image](../color-warning.png)

### [w015](#w015): Le dépôt git contient des changements non commit

Il y a des changements non commit dans le dépôt git. Les expériences avec le contexte '{context}' devraient toujours ne rien avoir à commit et devraient avoir un arbre de travail dévoué de 'git status'.



![color w image](../color-warning.png)

### [w016](#w016): La tâche run_orji est désactivée

La ressource de tâche '{resource_path}' a une valeur de catchup '{catchup}', mais les tâches 'run_orji' doivent toujours être activées. De cette façon, les utilisateurs peuvent décider de s'abonner ou se désabonner.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/orji)



![color w image](../color-warning.png)

### [w017](#w017): Le fichier d'archive n'est pas un lien

Le fichier archive '{bad}' n'est pas un lien. Les fichiers nommés '.protocole_*' ou '.archive_monitor_*' doivent être des liens pour que les développeurs du système d'archivage puissent gérer plus facilement leurs paramètres.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Archive)



![color w image](../color-warning.png)

### [w018](#w018): Mauvaises lignes dans gitignore

Le fichier gitignore '{gitignore_path}' ne doit pas contenir la ligne '{line}'. Il est important que ces patrons soient dans le dépôt git afin qu'une suite complète puisse être partagée.



![color w image](../color-warning.png)

### [w019](#w019): Lignes manquantes dans gitignore

Le fichier gitignore '{gitignore_path}' doit contenir {content}. Cette procédure permet d'ignorer les fichiers générés lors de l'exécution de cette suite qui ne font pas partie du projet sous contrôle de version.



![color w image](../color-warning.png)

### [w020](#w020): Aucun gitignore

Il devrait y avoir un fichier gitignore ici: '{gitignore_path}'. L'absence de '.gitignore' signifie que des grandes quantités de fichiers temporaires et sans importance peuvent être accidentellement partagés ou ajoutés au projet.



![color w image](../color-warning.png)

### [w021](#w021): Fichier maestro dans $CMCCONST

Le realpath pour le fichier '{path}' est dans le dossier $CMCCONST '{cmcconst}'. La tâche, la configuration, le flux et les fichiers ressources XML utilisés dans une suite ne devraient pas se trouver dans le dossier $CMCCONST.


[Plus d'info](https://gitlab.science.gc.ca/CMOI/best-practices/blob/master/fr/constants.md)



![color w image](../color-warning.png)

### [w022](#w022): Les ressources de la machine sont codés en dur

L'expérience semble avoir le contexte '{context}', mais la valeur de 'machine' '{machine_value}' dans '{resource_path}' est codée en dur. Un switchover peut briser cette suite. Utilisez une variable comme $FRONTEND à la place.



![color w image](../color-warning.png)

### [w023](#w023): Un utilisateur opérationnel peut écraser l'expérience

L'utilisateur opérationnel '{user}' a des droits d'écriture sur le fichier de projet permanent '{path}'. Pour plus de sécurité lors d'exécutions opérationnelles, '{user}' devrait seulement avoir des droits de lecture et d'exécution pour les fichiers de projet permanents.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/CMOI/Suite_Management_Process)



![color w image](../color-warning.png)

### [w024](#w024): Utilisateur parallèle dans le contexte opérationnel

L'expérience semble avoir le contexte '{context}', mais le fichier '{file_path}' fait référence au systèmes parallèles avec la chaîne de caractères '{par_string}'.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/CMOI/Suite_Management_Process)



![color w image](../color-warning.png)

### [w025](#w025): Fichiers opérationnels ont un propriétaire invalide

L'expérience semble avoir le contexte '{context}', mais le fichier '{path}' est la propriété de '{owner}' alors que '{expected}' est attendu.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/CMOI/Suite_Management_Process)



![color w image](../color-warning.png)

### [w026](#w026): Wallclock beaucoup plus grand qu'exigé par l'historique

Le wallclock pour '{node_path}' est '{wallclock_seconds}' secondes dans '{resource_xml}', mais la dernière exécution réussie sur '{datestamp}' a pris '{latest_seconds}' secondes. Ceci est {factor} fois plus grand (le seuil de ce message est {threshold}). Le wallclock devrait probablement être abaissé.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Wallclock)



![color w image](../color-warning.png)

### [w027](#w027): Paquets SSM écrasés

Différentes versions '{package}' du paquet SSM sont utilisées: {versions}. Tous doivent utiliser la même version, ou bien le paquet doit être seulement ajouté à l'environnement en un seul endroit. Fichiers:\n{paths}


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Ssm)



![color w image](../color-warning.png)

### [w028](#w028): Chemin ressources en suite sans datestamp

Le fichier de ressources '{resource_path}' a des références génériques aux chemins de projet maestro qui n'utilisent pas un horodatage comme '_20200401'. Cela peut causer des problèmes au dépannage, à l'historique et aux transferts. Chemins:\n{bad}


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/CMOI/Suite_Management_Process)



![color w image](../color-warning.png)

### [w029](#w029): Aucun lien symbolique vers le répertoire home opérationnel

Le chemin d'expérience '{target}' est l'endroit où les suites opérationnelles sont installées, mais il n'y a pas de lien symbolique '{source}' s'y référant.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/CMOI/Suite_Management_Process)



![color w image](../color-warning.png)

### [w030](#w030): Système uspmadt obsolète

Les options '-r' ou '-t' utilisées dans '{path}' avec 'fname', 'fgen+' ou 'dtstmp' ne devraient pas être une variable 'run'. Cette procédure utilise le système obsolète 'uspmadt'. Utilisez 'CMCSTAMP' à la place. Lignes:\n{lines}


[Plus d'info](https://gitlab.science.gc.ca/CMOI-Service-Desk/General/issues/5)



![color w image](../color-warning.png)

### [w031](#w031): Variables manquantes dans resources.def

Le fichier '{path}' doit définir les variables {required}, mais celles-ci étaient manquantes: {missing}


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/CMDI/Good_practices)



![color w image](../color-warning.png)

### [w032](#w032): Mauvais chemin de noeud de dépendance

Le fichier '{resource_path}' décrit un chemin de noeud '{node_path}' qui n'existe pas. Cette dépendance peut être ignorée.



![color w image](../color-warning.png)

### [w033](#w033): Mauvais chemin de noeud de dépendance relative

Le fichier '{resource_path}' a un attribute 'dep_name' '{dep_name}' qui décrit un chemin de nœud relatif '{node_path}' qui n'existe pas par rapport à '{node_folder}'. Il est possible que maestro ignore cette dépendance.



![color w image](../color-warning.png)

### [w034](#w034): Mauvais chemin de nœud de dépendance externe

Le fichier '{resource_path}' décrit un chemin de nœud '{node_path}' qui n'existe pas dans l'expérience '{exp}'. Cette dépendance peut être ignorée.



![color w image](../color-warning.png)

### [w035](#w035): Mauvais éxperience de dépendance externe

Le fichier '{resource_path}' décrit une expérience '{exp}' qui n'existe pas ou qui est brisée. Maestro pourrait ignorer cette dépendance.



![color w image](../color-warning.png)

### [w036](#w036): Pas d'archivage dans le hub

Le dossier 'hub' semble contenir des sous-dossiers contenant plus de {file_count} fichiers de plus de {day_count} jours mais pas de fichier d'archivage '.protocole_*'. Ces fichiers peuvent s'accumuler:\n{folders}



![color w image](../color-warning.png)

### [w037](#w037): Aucun flow XML pour module

Le module '{module_name}' n'a pas de fichier flow XML à '{flow_path}'. Le nom du module est défini dans ces XML:\n{define_paths}



![color w image](../color-warning.png)

# Niveau: Info
![color i image](../color-info.png)

### [i001](#i001): Plusieurs répertoires home pour le projet

L'expérience maestro se trouve dans le dossier d'accueil '{real_home}' mais les fichiers maestro principaux sont des liens symboliques vers d'autres utilisateurs. Cette procédure peut être instable. Liens:\n{bad_links}


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/User:Maciaa/home_references)



![color i image](../color-info.png)

### [i002](#i002): Nom du module et chemin différents

Le dossier du module '{folder_name}' diffère du nom du module '{attribute_name}' dans le flux XML '{xml_path}'. Ce nom peut aider l'organisation d'un projet, mais il peut aussi porter à confusion.



![color i image](../color-info.png)

### [i003](#i003): Fichiers d'échange d'éditeur de texte

Des fichiers d'échange d'un éditeur de texte ont été trouvés, par exemple, pour vim ou emacs. Si votre éditeur est fermé, il vous faudrait probablement les récupérer ou les supprimer:\n{swaps}


[Plus d'info](https://www.google.com/search?q=what+is+a+text+editor+swap+file)



![color i image](../color-info.png)

### [i004](#i004): Le dépôt git contient des changements non commit

Des changements qui n'ont pas été commit sont présents dans le dépôt git.



![color i image](../color-info.png)

### [i005](#i005): Signal inconnu de nodelogger

Un signal inconnu a été donné au nodelogger exécutable pour son argument '-s': {signals}. Ce message n'apparaîtra pas dans le centre de messages. Si vous ne voulez pas qu'il apparaisse dans le centre de message, utilisez 'infox'.\n{details}


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Nodelogger)



![color i image](../color-info.png)

### [i006](#i006): Développeurs principaux de l'historique git

Selon l'historique git pour ce projet (fréquence commit, récence, continuité), ceux-ci semblent être les développeurs principaux:\n{developers}



![color i image](../color-info.png)

### [i007](#i007): Chemins de développeurs absolus

Le fichier '{path}' contient des chemins absolus vers un répertoire d'utilisateur home non opérationnel, ce qui ne peut être installé de manière opérationnelle:\n{bad}



![color i image](../color-info.png)

### [i008](#i008): Ancienne version SSM

Le fichier '{path}' utilise un paquet SSM '{old}', mais une version plus récente '{new}' est disponible.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Ssm)



![color i image](../color-info.png)

### [i009](#i009): Ressources avec des heures valides

Les tâches de ce projet ont des configurations ressources qui changent selon l'heure. Recherchez les attributs 'valid_hour' ou 'valid_dow' attributs:\n{paths}


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/folders/resources)



![color i image](../color-info.png)

### [i010](#i010): Pas de hcrons

Aucun fichier de configuration actif pour '{suite_name}' trouvé dans '{hcron_folder}'. La suite pourrait ne pas s'exécuter automatiquement.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Hcron)



![color i image](../color-info.png)

# Niveau: Meilleures pratiques
![color b image](../color-best-practice.png)

### [b001](#b001): Chemin de dépendance codé en dur

L'attribut 'exp' '{exp_value}' dans le fichier ressources '{resource_path}' est codé en dur. Ce type de code est moins portable et plus fragile. Pensez plutôt à utiliser une variable.



![color b image](../color-best-practice.png)

### [b002](#b002): "SupportInfo" trop long

L'attribut SupportInfo dans le fichier XML '{xml_path}' contient '{char_count}' caractères, mais il devrait en contenir moins. Le nombre maximum recommandé de caractères est {max_chars}.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/ExpOptions.xml)



![color b image](../color-best-practice.png)

### [b003](#b003): "SupportInfo" n'a pas de URL

L'attribut SupportInfo dans le fichier XML '{xml_path}' doit contenir une URL. Il s'agit de la méthode recommandée pour fournir des étapes de dépannage et des détails sur l'état du support.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/ExpOptions.xml)



![color b image](../color-best-practice.png)

### [b004](#b004): Texte "SupportInfo" non standard

L'attribut SupportInfo dans le fichier XML '{xml_path}' doit commencer par une valeur comme 'Full Support'. Débuts de chaînes recommandés:\n{substrings}


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/ExpOptions.xml)



![color b image](../color-best-practice.png)

### [b005](#b005): Fichier ou dossier obsolète

Le fichier ou le dossier '{path}' est obsolète et devrait être supprimé.



![color b image](../color-best-practice.png)

### [b006](#b006): Des fichiers autres que maestro dans des dossiers maestro

Le dossier maestro '{folder}' ne doit contenir que des fichiers maestro comme tsk, cfg, xml. Ces fichiers ont été trouvés dans:\n{filenames}


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro_Files_and_Folders)



![color b image](../color-best-practice.png)

### [b007](#b007): Commentaires dans le fichier de configuration

La section pseudo-xml (qui contient 'input', 'executables', et 'output') du fichier de configuration '{file_path}' a {count} lignes de commentaires commençant par '##'. Celles-ci semblent être des lignes de configuration et non des commentaires, et devraient être supprimées.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/.cfg)



![color b image](../color-best-practice.png)

### [b008](#b008): Utilisation de file d'attente alias cachée

La valeur '{value}' pour la variable ressource '{name}' est une file d'attente alias cachée et n'est pas facilement visible dans qstat. Pensez plutôt à utiliser une de ces files d'attente:\n{queues}



![color b image](../color-best-practice.png)

### [b009](#b009): Attribut obsolète dans un élément SUBMITS

Le fichier '{xml_path}' a l'attribut 'type' dans un élément SUBMITS. Cet attribut est obsolète et peut être supprimé.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/SUBMITS)



![color b image](../color-best-practice.png)

### [b010](#b010): Caractères non standards dans le nom exécutable

Le chemin de configuration '{bad}' dans '{config_path}' contient des caractères non standards qui ne correspondent pas à la regex '{regex}'. Ces caractères peuvent causer des bogues avec les outils, les systèmes d'exploitation et les parseurs. Pensez à les renommer. {dollar_msg}


[Plus d'info](https://regex101.com/)



![color b image](../color-best-practice.png)

### [b011](#b011): Référence dans la racine du fichier ou dossier CMCCONST

Il existe des références à des fichiers ou dossiers dans la racine du dossier CMCCONST. Il est recommandé d'utiliser plutôt des dossiers dans CMCCONST. Le fichier '{file_path}' contient:\n{matching_string}


[Plus d'info](https://gitlab.science.gc.ca/CMOI/best-practices/blob/master/fr/constants.md)



![color b image](../color-best-practice.png)

### [b012](#b012): Fichier d'archive obsolète

Le fichier '{bad}' est un fichier d'archive obsolète et doit être supprimé. Si le hubn'a pas d'autres fichiers d'archive, son état d'archivage devrait être examiné.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Archive)



![color b image](../color-best-practice.png)

### [b013](#b013): Nom de dossier vague

Un lien symbolique descriptif devrait accompagner le dossier '{unclear}', par exemple 'forecast -> e1'. En ayant recours à cette méthode, les débutants ont une chose de moins à mémoriser.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Obfuscation)



![color b image](../color-best-practice.png)

### [b014](#b014): Nom du nœud incorrect

Le nom du noeud '{node_name}' est incorrect en flux XML '{flow_path}'. Ce nom fonctionne, mais il est recommandé de faire correspondre la regex suivante: {regex}


[Plus d'info](https://regex101.com/)



![color b image](../color-best-practice.png)

### [b015](#b015): Variables non standards dans les ressources BATCH

L'attribut '{attribute_name}' dans l'élément BATCH dans '{resource_path}' utilise une variable non standard '{attribute_value}'. Pensez plutôt à utiliser une variable standard pour rendre la configuration plus facile à suivre et à modifier: {recommended}


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Maestro/folders/resources)



![color b image](../color-best-practice.png)

### [b016](#b016): Propriétaires ou permissions de fichiers irréguliers

Le fichier '{path}' est le propriétaire et possède les permissions '{ugp}', mais la plupart des fichiers d'expérience ont '{expected}'. Le propriétaire, le groupe et les permissions des fichiers d'expérience permanents devraient être les mêmes.



![color b image](../color-best-practice.png)

### [b017](#b017): SEQ_ trouvé au lieu de MAESTRO_

La variable obsolète '{old}' a été trouvée dans '{path}'. Pensez plutôt à utiliser la variable équivalente '{new}'. En voyant que cette variable est liée à maestro, les débutants pourront mieux comprendre votre projet.



![color b image](../color-best-practice.png)

### [b018](#b018): La variable en majuscules est définie plus d'une fois

La variable en majuscules '{variable}' est définie plus d'une fois dans le fichier '{path}'. Les variables en majuscules dans BASH doivent être considérées comme constantes, et ne doivent pas être changées. Notez que dans d'autres langages de programmation, "const" ne peut être défini qu'une seule fois.


[Plus d'info](https://google.github.io/styleguide/shellguide.html#s7.3-constants-and-environment-variable-names)



![color b image](../color-best-practice.png)

### [b019](#b019): "Readme" n'est pas en markdown

Le fichier '{readme}' ressemble à un fichier 'readme'. Pensez à l'écrire en markdown et à le renommer en '{suggested}' pour qu'il apparaisse déjà formaté et de manière automatique sur des plateformes comme GitLab.


[Plus d'info](https://www.youtube.com/watch?v=SCAfcuQ0dBE)



![color b image](../color-best-practice.png)

### [b020](#b020): Literalpath  au lieu de CMCPROD

Des literalpaths à 'hall3' ou 'hall4' ont été trouvés. Pensez plutôt à utiliser la variable CMCPROD afin que votre projet suive les configurations switchover. Le fichier '{file_path}' contient:\n{matching_string}


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Cmcprod)



![color b image](../color-best-practice.png)

### [b021](#b021): Chemin absolu dans la configuration au lieu de getdef

Le fichier de configuration '{config}' doit utiliser getdef au lieu de chemins absolus codés en dur. Exemple: ABC=$(getdef resource ABC)\nChemins absolute:\n{values}


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/User:Lims/projects/tech_transfer_improvements#Making_variables_configurable_from_high_level)



![color b image](../color-best-practice.png)

### [b022](#b022): Fichiers identiques dans le module

Les fichiers dans le même module sont identiques. Pensez à remplacer les fichiers en double avec des liens symboliques pour qu'une correction ou mise à jour puisse s'appliquer à tous. Sinon, pensez à distinguer les fichiers avec des commentaires. Fichiers:\n{paths}


[Plus d'info](https://fr.wikipedia.org/wiki/Ne_vous_r%C3%A9p%C3%A9tez_pas)



![color b image](../color-best-practice.png)

### [b023](#b023): Variables etiket non définies dans etikets.cfg

Les variables etiket {etikets} sont définies dans '{bad_path}', mais elles devraient être définies dans '{good_path}'. Les variables etiket sont découvertes par ce scanner en fonction de leur nom 'ETIK' et de l'utilisation de 'pgsm' et 'editfst'.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/Etikets.cfg)



![color b image](../color-best-practice.png)

### [b024](#b024): Variables de configurations définies, mais inutilisées

Le fichier de configuration '{config}' définit les variables '{unused}', mais celles-ci ne sont pas utilisées. Pensez à les supprimer.



![color b image](../color-best-practice.png)

### [b025](#b025): Origine de git remote incorrecte

L'expérience semble avoir le contexte '{context}', mais le git remote 'origin' pointe vers '{bad}'. Le remote devrait plutôt commencer par '{good}'.



![color b image](../color-best-practice.png)

### [b026](#b026): Recommandations pour le script shell

La commande '{verify_cmd}' recommande d'effectuer des changements dans le script shell '{path}'. Sortie:\n{output}



![color b image](../color-best-practice.png)

### [b027](#b027): Chemin non-standard de commande  maestro

La commande '{cmd}' dans '{path}' pour l'outil maestro '{tool}' doit être précédée par '{prefix1}' ou '{prefix2}', tout comme '{expected}'.



![color b image](../color-best-practice.png)

### [b028](#b028): Mauvaise cible de EntryModule

La cible pour le lien '{path}' doit être '{good}' au lieu de '{bad}'.


[Plus d'info](https://wiki.cmc.ec.gc.ca/wiki/CMDI/Good_practices)



![color b image](../color-best-practice.png)

### [b029](#b029): Lien products_dbase obsolète

Le dossier '{bad}' dans 'hub' ne doit pas être un lien. Ce style de dossier de produits est obsolète.


[Plus d'info](https://gitlab.science.gc.ca/CMOI/maestro/issues/219)



![color b image](../color-best-practice.png)

### [b030](#b030): Mauvais nom de variable de domaine SSM

Le fichier '{path}' utilise des variables comme domaines SSM, mais les noms ne commencent pas par 'SSM _':\n{variables}



![color b image](../color-best-practice.png)

### [b031](#b031): Exécutable compilé dans le projet

Le fichier "{path}" semble être un exécutable compilé. Ceux-ci appartiennent à l'extérieur du projet maestro, ou peut-être dans un package SSM.



![color b image](../color-best-practice.png)

### [b032](#b032): Fichier inutilisé dans le dossier bin

Le fichier '{path}' dans le dossier 'bin' n'est utilisé dans aucun fichier 'cfg' ou 'tsk'. Pensez à le supprimer du projet.


![color b image](../color-best-practice.png)

# Page Générée

Cette page a été générée par le script 'generate_messages_markdown.py' du repo 'https://gitlab.science.gc.ca/CMOI/maestro' le '2021-02-25'.
